# DaisySuite

DaisySuite is a pipeline for horizontal gene transfer (HGT) detection using sequencing data.

[Documentation](http://daisysuite.readthedocs.io/en/latest/)

Please find the DaisySuite Repository at [DaisySuite](https://gitlab.com/eseiler/DaisySuite)
